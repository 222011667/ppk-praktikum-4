Berikut adalah hasil deploy dokumentasi API menggunakan Swagger-UI yang dapat diakses di [link berikut](http://himpunanmasalah.great-site.net/ppk-praktikum-4/)

![Hasil deploy](Dokumentasi/11.png)

Berikut daftar Routes yang dapat diakses dari localhost:8080

![Postman](Dokumentasi/1.png)

*Berikut adalah hasil pengujian dengan Postman menggunakan localhost:8080*

Pengujian Get All Product

![Postman](Dokumentasi/2.png)

Pengujian Get Product by Id

![Postman](Dokumentasi/3.png)

Karena ada kendala sintax yang diberikan di module, di mana getVar() ternyata me-return null, kode program yang menggunakan getVar() diubah menggunakan getRawInput() yang mana me-return array berisi data yang dikirimkan. 

Selain itu, baris yang mengubah $id juga dihapus, karena getVar() yang me-return null membuat $id menjadi null sehingga saat update, seluruh data ikut berubah. Solusinya adalah dengan membiarkan $id apa adanya berdasarkan parameter fungsi.

![Postman](Dokumentasi/4.png)

Pengujian Update dan Get Product by Id

![Postman](Dokumentasi/5.png)
![Postman](Dokumentasi/6.png)

Pengujian Create dan Get All Product

![Postman](Dokumentasi/7.png)
![Postman](Dokumentasi/8.png)

Pengujian Delete dan Get All Product

![Postman](Dokumentasi/9.png)
![Postman](Dokumentasi/10.png)


# CodeIgniter 4 Application Starter

## What is CodeIgniter?

CodeIgniter is a PHP full-stack web framework that is light, fast, flexible and secure.
More information can be found at the [official site](http://codeigniter.com).

This repository holds a composer-installable app starter.
It has been built from the
[development repository](https://github.com/codeigniter4/CodeIgniter4).

More information about the plans for version 4 can be found in [the announcement](http://forum.codeigniter.com/thread-62615.html) on the forums.

The user guide corresponding to this version of the framework can be found
[here](https://codeigniter4.github.io/userguide/).

## Installation & updates

`composer create-project codeigniter4/appstarter` then `composer update` whenever
there is a new release of the framework.

When updating, check the release notes to see if there are any changes you might need to apply
to your `app` folder. The affected files can be copied or merged from
`vendor/codeigniter4/framework/app`.

## Setup

Copy `env` to `.env` and tailor for your app, specifically the baseURL
and any database settings.

## Important Change with index.php

`index.php` is no longer in the root of the project! It has been moved inside the *public* folder,
for better security and separation of components.

This means that you should configure your web server to "point" to your project's *public* folder, and
not to the project root. A better practice would be to configure a virtual host to point there. A poor practice would be to point your web server to the project root and expect to enter *public/...*, as the rest of your logic and the
framework are exposed.

**Please** read the user guide for a better explanation of how CI4 works!

## Repository Management

We use GitHub issues, in our main repository, to track **BUGS** and to track approved **DEVELOPMENT** work packages.
We use our [forum](http://forum.codeigniter.com) to provide SUPPORT and to discuss
FEATURE REQUESTS.

This repository is a "distribution" one, built by our release preparation script.
Problems with it can be raised on our forum, or as issues in the main repository.

## Server Requirements

PHP version 7.4 or higher is required, with the following extensions installed:

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (enabled by default - don't turn it off)
