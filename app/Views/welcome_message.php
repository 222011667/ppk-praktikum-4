<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap 5.2 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- bootstrap 5.2 CSS -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">

    <title>PPl - Praktikum 4</title>

    <style>
        body{
            background-color: rgba(50, 65, 70, 1.0);
            color: white;
            font-family: 'Ubuntu', sans-serif;
        }
        .card p, .card h5{
            /*color: saddlebrown;*/
        }
        .card{
            background-color: black;
            height: 100%;
        }
        a{
            text-decoration: none;
            color: thistle;
        }
        #rss-tempo{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-cnn{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-republika{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #json-gempa{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-tempo h2{

        }

        /*Moving text banner animation*/
        #scroll-container {
          /*border: 3px solid black;*/
          background: rgba(0, 0, 0, 0.1);
          padding: 1em;
          border-radius: 5px;
          overflow: hidden;
        }

        #scroll-text {
            white-space: nowrap;
          /* animation properties */
          -moz-transform: translateX(100%);
          -webkit-transform: translateX(100%);
          transform: translateX(100%);
          
          -moz-animation: my-animation 25s linear infinite;
          -webkit-animation: my-animation 25s linear infinite;
          animation: my-animation 25s linear infinite;
        }

        /* for Firefox */
        @-moz-keyframes my-animation {
          from { -moz-transform: translateX(100%); }
          to { -moz-transform: translateX(-150%); }
        }

        /* for Chrome */
        @-webkit-keyframes my-animation {
          from { -webkit-transform: translateX(100%); }
          to { -webkit-transform: translateX(-150%); }
        }

        @keyframes my-animation {
          from {
            -moz-transform: translateX(100%);
            -webkit-transform: translateX(100%);
            transform: translateX(100%);
          }
          to {
            -moz-transform: translateX(-150%);
            -webkit-transform: translateX(-150%);
            transform: translateX(-150%);
          }
        }
        /*Moving text banner animation*/
        
        /*Fade in Fade out*/
        .fade{
            animation: fadeIn 2s;
        }
        @keyframes fadeIn {
          0% { opacity: 0; }
          100% { opacity: 1; }
        }
        /*Fade in Fade out*/
        .goyang{
            transition: transform .2s; /* Animation */
            user-select: none; /* supported by Chrome and Opera */
           -webkit-user-select: none; /* Safari */
           -khtml-user-select: none; /* Konqueror HTML */
           -moz-user-select: none; /* Firefox */
           -ms-user-select: none;
        }
        .goyang:hover{
            transform: rotate(5deg);
        }
        .accordion-item button{
            background: rgba(0, 50, 50, 0.2);
        }
    </style>

<!-- HTML for static distribution bundle build Swagger UI -->
    <link rel="stylesheet" type="text/css" href="swagger-ui/dist/swagger-ui.css" />
    <link rel="stylesheet" type="text/css" href="swagger-ui/dist/index.css" />
    <link rel="icon" type="image/png" href="swagger-ui/dist/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="swagger-ui/dist/favicon-16x16.png" sizes="16x16" />

</head>



<body>
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark fw-semibold fs-5">
        <div class="container">
            <a class="navbar-brand goyang fs-3" href="#">PPK - Praktikum 4</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="<?= base_url() ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-dark" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>

    <!-- Swagger UI -->
    <div id="swagger-ui"></div>
    <script src="swagger-ui/dist/swagger-ui-bundle.js" charset="UTF-8"> </script>
    <script src="swagger-ui/dist/swagger-ui-standalone-preset.js" charset="UTF-8"> </script>
    <script src="swagger-ui/dist/swagger-initializer.js" charset="UTF-8"> </script>
    <!-- Swagger UI -->


    <script>
        
    </script>



    <!-- bootstrap 5.2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <!-- bootstrap 5.2 JS -->
</body>

</html>